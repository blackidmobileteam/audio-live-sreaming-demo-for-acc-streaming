package com.example.audiotesting;

import com.spoledge.aacdecoder.MultiPlayer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends Activity {
	
	Button playBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		playBtn	= (Button)findViewById(R.id.play_btn);
		
		final MultiPlayer multiPlayer = new MultiPlayer();
		playBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				multiPlayer.playAsync("http://icy2.abacast.com/igroove-igrooveaac-64?type=.mp3&source=v5player");
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
